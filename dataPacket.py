from packet import Packet
import base64
import datetime

#Data Packet sent by server to client

class DataPacket(Packet):

    def __init__(self,data,seq,timeout):
        self.acked = False
        self.sent = False
        self.packetType = "data"
        self.seq = seq
        self.data = base64.b64encode(data)
        self.expiryTime = None

