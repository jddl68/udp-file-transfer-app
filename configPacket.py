import settings
from packet import Packet

#Config Packet send to the server to configure it

class ConfigPacket(Packet):
    
    filename = "sample.txt"
    chunksize = 0
    windowsize = 0
    isn = 0
    timeout = 0
    delay = 0
    ip = ""
    port = 0

    def __init__(self, filename_param, chunksize_param, windowsize_param, isn_param, timeout_param, delay_param, ip_param, port_param):
        self.packetType = "config"
        self.filename = filename_param
        self.chunksize = int(chunksize_param)
        self.windowsize = windowsize_param
        self.isn = isn_param
        self.timeout = timeout_param
        self.delay = delay_param
        self.ip = ip_param
        self.port = int(port_param)

