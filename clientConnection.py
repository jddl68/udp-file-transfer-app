import settings
import socket
from configPacket import ConfigPacket
from packetFactory import PacketFactory
from ackPacket import AckPacket
import datetime

#This class is the unreliable UDP pseudo-connection to the server

class ClientConnection:
    ip = ""
    port = 0
    sock = None
    chunksize = 0


    def __init__(self, ip_param, port_param, chunksize_param):
        self.ip = ip_param
        self.port = int(port_param)
        self.chunksize = chunksize_param
        if settings._verbose:
            print "[SOCK] Creating UDP socket at port {}".format(port_param)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, 65535)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 65535)

    def sendPacket(self,packet_param): #Sends packet to server
        if settings._verbose:
            print "{} [SEND] Sending ACK to server ({})".format(datetime.datetime.now(),packet_param.seq)

        self.sock.sendto(packet_param.toJSON() + "\n", (self.ip, self.port))

    def receivePacket(self): #Returns Packet reccieved
        data =  self.sock.recv(65536)
        packet = PacketFactory.packetWithJSON(data) #Transforms JSON data received back to its Packet object

        if settings._verbose:
            print "{} [RECV] Packet Received({})".format(datetime.datetime.now(),packet.seq)

        return packet, len(data)