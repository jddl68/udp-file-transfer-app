from ackPacket import AckPacket
from configPacket import ConfigPacket
from dataPacket import DataPacket 
import json



class PacketFactory:
    '''
        class in charge of creating packets from JSON Data
    '''
    @classmethod
    def packetWithJSON(self,jsonData):
        
        ''' 
            packetWithJSON(jsonData)
            Converts JSON to proper Packet Sub-class based on packet type 

            @params
                jsonData - packet in JSON format
            @returns
                A Packet in its Python object form
        '''

        jsonDict = json.loads(jsonData)

        if jsonDict['packetType'] == 'config':
            return ConfigPacket.initWithDict(jsonDict)
        elif jsonDict['packetType'] == 'ack':
            return AckPacket.initWithDict(jsonDict)
        elif jsonDict['packetType'] == 'data':
            return DataPacket.initWithDict(jsonDict)