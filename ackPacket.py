import settings
from packet import Packet

#Ack Packet used to acknowledge chunks, signal start of file transfer and end of file transfer

class AckPacket(Packet):
    
    def __init__(self, seq_param):
        self.packetType = "ack"
        self.seq = seq_param

    
