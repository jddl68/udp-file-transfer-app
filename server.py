import SocketServer
import getopt
import sys
import filemanager
import os
import socket
import datetime

import settings
from ackPacket import AckPacket
from configPacket import ConfigPacket
from packetFactory import PacketFactory
from packetManager import DataPacketManager




class UDPHandler(SocketServer.BaseRequestHandler):

    '''
        Class that handles all packets that the server receives.
        It reacts depending on the type of packet it received.

        The way it handles received packets are mostly derived from
        the reliable data transfer mechanics discussed in the lecture
    '''

    transferEnded = False

    def handle(self):
        jsonData = self.request[0].strip()
        mySocket = self.request[1]
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, 65535)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 65535)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        packet = PacketFactory.packetWithJSON(jsonData)
        self.transferEnded = False


        #Handle Config Packet Received
        if packet.packetType == "config" :
            print "\n[CONNECT] Client connected, config received "
           
            #get reference of config packet
            config = packet
            UDPHandler.config = property(lambda self:config)

            print "\n======== Server Configuration ============="
            print "Filename: " + self.config.filename
            print "Chunksize: " + str(self.config.chunksize) + "bytes"
            print "Windowsize: " + str(self.config.windowsize * self.config.chunksize) + "bytes (" + str(self.config.windowsize) + " chunks)"
            print "Initial Sequence Number: " + 'isn'
            print "Timeout: " + str(self.config.timeout) + "ms"
            print "Delay: " + str(self.config.delay) + "ms"
            print "Address: " + self.config.ip + ":" + str(self.config.port)
            
            
            print "\n[FILE] Preparing File for sending"
            try:

                #Split the files accordiing to the config    
                splittedFile = filemanager.split_file(config.filename,config.chunksize)

                #Initialize DataPacketManager
                dp = DataPacketManager(mySocket,self.client_address,config,splittedFile)
                
                #Add DataPacketManager to UDPHandler class
                UDPHandler.manager = property(lambda self:dp)
              

                #Send Ack Packet to client, in response to receiving the config
                if settings._verbose:
                    print "[SEND] Sending ACK Packet for config, waiting for ack to start file transfer"

                mySocket.sendto(AckPacket(self.manager.dataPackets[-1].seq).toJSON(), self.client_address)
                self.transferEnded = False

            except IOError:
                print "[ERROR] File does not exist, sending ACK(-1)"
                mySocket.sendto(AckPacket(-1).toJSON(), self.client_address)
					
        #Handle AckPacket Received from client
        elif packet.packetType == "ack" and self.transferEnded == False:
            if settings._verbose:
                print "{} [RECV] Received ACK packet ({})".format(datetime.datetime.now(),packet.seq)

            if packet != None:
                if packet.seq == self.config.isn:
                    #Initial request for data received
                    #Send packets in window
                    print "\n[SEND] Transfering {} to client".format(self.config.filename)
                    self.manager.sendPacketsInWindow()
                    #self.manager.expectedSeq = self.manager.expectedSeq + self.config.chunksize;
                    #print "Head now at {}".format(self.manager.window.get_first().seq)
                elif self.manager.expectedSeq == packet.seq:
                    #expectedSeq is equal to packet.seq
                    #slide the window

                    self.manager.stopTimeoutManager()
                    self.manager.slideWindow()
                    self.manager.expectedSeq = self.manager.expectedSeq+self.config.chunksize
                    self.manager.startTimeoutManager()


                    if packet.seq == self.manager.dataPackets[-1].seq + self.config.chunksize and self.transferEnded == False:
                        #if this evaluates, it signals that file transfer has ended

                        self.transferEnded = True
                        self.manager.stopTimeoutManager()

                        #Send ACK(0) to client to signal end of file transfer
                        #mySocket.sendto(AckPacket(0).toJSON(), self.client_address)

                        if settings._verbose:
                            print "\n[EOF] Sending ACK to client({})".format(self.manager.expectedSeq - self.config.chunksize)

                        print "[SEND] File sent to client"
                        print "\n========= Transmission Statistics =========="
                        print "File Sent: {} ({} bytes)".format(self.config.filename, os.path.getsize(self.config.filename))
                        print "Packets Sent: {}".format(self.manager.packetsSent)
                        print "Timeouts: {}\n".format(self.manager.timeouts)

                        print "[SERVER] Server listening, waiting for clients"
                else:
                    pass
                    #print "Packet {} Ignored".format(packet.seq)
def main(argv):
    port = 50000

    #Get arguments
    try:
        opts, args = getopt.getopt(argv, "p:v")
    except getopt.GetoptError:
        print "[ERROR] Invalid Arguments"
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-p':
            port = int(arg)
        if opt == '-v':
            settings._verbose = True


    HOST, PORT = "0.0.0.0", port
    
    #Create UDP Server with specified port in localhost
    print "[SERVER] Creating a UDP Server at port {}".format(port)
    server = SocketServer.UDPServer((HOST, PORT), UDPHandler,bind_and_activate=False)
    myMax_packet_size = 8192*2
    server.max_packet_size = myMax_packet_size
    #print server.max_packet_size
    server.allow_reuse_address  = True
    server.server_bind()
    server.server_activate()
    print "[SERVER] Server listening, waiting for clients"
    #print server.max_packet_size
    server.serve_forever()

if __name__ == "__main__":
    main(sys.argv[1:])

