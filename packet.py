import json
import settings



class Packet:
    '''
        Abstract Packet Class where all packets derive from
    '''
    
    seq = 0
    packetType = ''

    def toJSON(self):
        j = json.dumps(self,skipkeys=True, default=lambda o: o.__dict__ if hasattr(o,'__dict__') else None, sort_keys=True, indent=4)
        return j

    @classmethod
    def initWithDict(cls,packetDict):
        for key in packetDict:
            setattr(cls, key, packetDict[key])
        return cls

