
def split_file(filename,blocksize):

    '''
    split_file(fp,blocksize)
        
    @params
        filename - name of the file to be split
        blocksize - size of each file chunk in bytes

    @return value
        result - array of file chunks sized to the given blocksize

    '''

    fp = open(filename,'rb')

    result = []
    
    for block in iter(lambda: fp.read(blocksize), ''):
        
        result.append(block)
    
    fp.close()
    
    return result
