'''
Created on Dec 10, 2013

@author: rptavas
'''
class Stack:
    def __init__(self) :
        self.data  = [];
    def push (self,data):
        self.data.insert(0,data);
    def pop (self):
        return self.data.pop(0) if len(self.data) != 0 else None;
    def isEmpty (self):
        return     (self.data ==[]);
    def count (self):
        return len(self.data)
    def get_first(self):
        x = None

        try:
            x = self.data[0]
        except Exception, e:
            pass

        return x
        

class Queue:
    def __init__(self) :
        self.data  = [];
    def push (self,data):
        self.data.append(data);
    def pop (self):
        return self.data.pop(0) if len(self.data) != 0 else None;
    def isEmpty (self):
        return     (self.data ==[]);
    def count (self):
        return len(self.data)
    def get_first(self):
        x = None

        try:
            x = self.data[0]
        except Exception, e:
            pass
            
        return x

class PriorityQueue:
    def __init__(self) :
        self.data  = [];
    def insert (self,data):
        self.data.append(data);
        self.data.sort(key=lambda x:x[2]);
    def delete (self):
        return self.data.pop(0);
    def isEmpty (self):
        return     (self.data ==[]);    
    def getAll (self):
        return self.data;