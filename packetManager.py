from __future__ import division
from dataPacket import DataPacket
from DataStruct import Queue
import datetime
from threading import Thread
import time

import settings

class DataPacketManager:

    '''
        DataPacketManager is a class that manages anything that is data packet related 
            - It handles the creation of DataPacket objects
            - It sends packets to the client (pipelined through a window)
            - The window of data packets is maintained based on the Go Back-N mechanics of reliable
            data transfer
        
    '''

    #Statistics Variables
    packetsSent = 0

    def __init__(self,socket,address,config,splittedFile):
        self.dataPackets = []
        self.window = Queue()
        self.config  = config
        self.address = address
        self.socket = socket
        self.index = config.windowsize-1 #keeps track of the index of the last dataPacket sent
        self.firstSend = False
        self.expectedSeq = self.config.isn + self.config.chunksize
        self.timeouts = 0

        #Create DataPackets
        for index,elem in enumerate(splittedFile):
            dp = DataPacket(elem,(index*config.chunksize)+config.isn,config.timeout)
            self.dataPackets.append(dp)

        #Initialize Window with the number of packets dictated by the windowsize
        self.window.data = self.dataPackets[0:config.windowsize]


    def sendPacketsInWindow(self):
        ''' sendPacketsInWindow()
            Send all data packets that are inside the window.
        '''

        for dataPacket in self.window.data:
            dataPacket.expiryTime = time.time() + (self.config.timeout/1000)
            
            if settings._verbose:
                print "{} [SEND] Sent DATA packet ({})".format(datetime.datetime.now(),dataPacket.seq)
            
            self.socket.sendto(dataPacket.toJSON(),self.address)
            
            dataPacket.sent = True
            self.packetsSent = self.packetsSent + 1

        #Detect First Send
        if not self.firstSend:
            #initialize timeout manager    
            self.firstSend = True
            self.startTimeoutManager()

    def slideWindow(self):
        '''
            slideWindow()
                this function uses a Queue data structure to be able
                to simulate a sliding window of data packets
        '''
        #Remove Data Packet that is first in the queue 
        self.window.pop()
        #Push a Data Packet into the window
        if self.index+1 <= len(self.dataPackets)-1:
            self.window.push(self.dataPackets[self.index+1])

            dataPacket = self.dataPackets[self.index+1]
            if settings._verbose:
                print "{} [SEND] Sent DATA packet ({})".format(datetime.datetime.now(),dataPacket.seq)
            self.socket.sendto(dataPacket.toJSON(),self.address)
            #dataPacket.expiryTime = datetime.datetime.now() + datetime.timedelta(milliseconds=self.config.timeout)
            dataPacket.expiryTime = time.time() + (self.config.timeout/1000)
            self.packetsSent = self.packetsSent + 1

        self.index = self.index + 1

    def startTimeoutManager(self):
        self.tm = TimeoutManager(self)
        self.tm.start()

    def stopTimeoutManager(self):
        '''
            Stops the timeout manager thread
        '''
        
        self.tm.manageWindow = False

class TimeoutManager(Thread):
    '''
        This class manages the data packet's expiry time. 
        If it detects that a data packet has expired,
        It invokes the sendPacketsInWindow() function
        of the DataPacketManager
    '''

    def __init__(self,packetManager):
        Thread.__init__(self)
        self.manageWindow = True 
        self.packetManager = packetManager
        self.window = packetManager.window

    def run(self):
        
        while self.manageWindow:

            if self.window.count != 0:

                if self.window.isEmpty() == True:
                    break

                obs_packet = self.window.get_first() #packet to be observed
                
                if obs_packet != None and obs_packet.expiryTime != None:
                    if time.time() > obs_packet.expiryTime:
                        self.packetManager.sendPacketsInWindow()
                        self.packetManager.timeouts = self.packetManager.timeouts + 1
                        if settings._verbose:
                            print "{} [TIMEOUT] DATA packet timed out ({})".format(datetime.datetime.now(),obs_packet.seq)
