from __future__ import division
from time import sleep
import sys
import getopt
import string
import base64
import time
import os
import random

#User Created
import settings
from threading import Timer
from ackPacket import AckPacket
from configPacket import ConfigPacket
from clientConnection import ClientConnection

pending = 0

def reply(ackN, clientConnection):
    global pending
    ackPacket = AckPacket(ackN)
    clientConnection.sendPacket(ackPacket)
    pending = pending - 1

def secondsToStr(t): #Converts seconds to formatted string
    return "%d:%02d:%02d.%03d" % \
        reduce(lambda ll,b : divmod(ll[0],b) + ll[1:],
            [(t*1000,),1000,60,60])   

def main(argv):
    #Local Variables
    #Configuration Variables
    filename = "sample.txt"
    chunksize = 512
    windowsize = 2
    isn = 10
    timeout = 500
    delay = 250
    ip = "localhost"
    port = 50000
    windowbyte = 1024

    #Statistics Variables
    packetsReceived = 0
    incorrectPackets = 0
    lastPacketSeq = 0
    startTime = None
    goodData = 0
    totalData = 0

    global pending
    pending  = 0
    random.seed()
    fileData = ""

    #Get Args
    try:
        opts, args = getopt.getopt(argv, "f:c:w:i:t:d:r:v")
    except getopt.GetoptError:
        print "[ERROR] Invalid Arguments"
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-f':
            filename = arg
        elif opt == '-c':
            chunksize = int(arg)
        elif opt == '-w':
            windowbyte = int(arg)
        elif opt == '-i':
            isn = int(arg)
        elif opt == '-t':
            timeout = int(arg)
        elif opt == '-d':
            delay = int(arg)
        elif opt == '-r':
            ip, port = arg.split(":", 1)
        elif opt == '-v':
            settings._verbose = True

    windowsize = int(windowbyte/chunksize)

    print "======== Client Configuration ============="
    print "Filename: " + filename
    print "Chunksize: " + str(chunksize)
    print "Windowsize: " + str(windowsize*chunksize) + "bytes (" + str(windowsize) + " chunks)"
    print "Initial Sequence Number: " + 'isn'
    print "Timeout: " + str(timeout) + "ms"
    print "Delay: " + str(delay) + "ms"
    print "Address: " + ip + ":" + str(port)

    #Initialize Pseudo-Connection
    clientConnection = ClientConnection(ip, port, chunksize)

    #Configure Server
    print "\n[CONFIG] Configuring Server"

    configPacket = ConfigPacket(filename,chunksize,windowsize,isn,timeout,delay,ip,port)
    clientConnection.sendPacket(configPacket)

    if settings._verbose:
        print "[CONFIG] Config packet send, waiting for reply \n"

    print "[REQ] Requesting {} from {}:{}".format(filename,ip,port)

    r = clientConnection.receivePacket()
    packet = r[0]
    if packet.packetType == "ack": #Receive ACK from server
        if packet.seq == -1:
            print "[CONFIG] File does not exist on server, exiting"
            sys.exit(2)
        else:
            if settings._verbose:
                print "[CONFIG] ACK{} received, sending ACK{} to signal start of file transfer".format(packet.seq, isn)
            lastPacketSeq = packet.seq 
            print lastPacketSeq       
    else:
        print "[CONFIG] Data received not an ACK Packet"

    #File Transfer
    print "\n[RECV] File Transfer Started".format(filename,ip,port)
    #Send ACK<ISN> to signal start of transfer

    ackPacket = AckPacket(isn)
    expectedSeq = isn
    clientConnection.sendPacket(ackPacket)
    
    #File Receive Proper
    startTime = time.time()
    while True:
        r = clientConnection.receivePacket()
        packet = r[0]
        packetLen = r[1]

        if random.randint(1,100) < 0 :
           continue

        packetsReceived = packetsReceived + 1
        if packet.packetType == 'data' : #Data Packet Receive
            totalData += packetLen
            if packet.seq == expectedSeq: #Write to file if Seq of packet is expected one
                #if delay != 0:
                    #sleep(delay/1000)
                #ackPacket = AckPacket(packet.seq+configPacket.chunksize)
                #clientConnection.sendPacket(ackPacket)
                if delay != 0:
                    t = Timer(delay/1000, reply, [packet.seq+configPacket.chunksize, clientConnection])
                    t.start()
                    pending = pending + 1
                else:
                    ackPacket = AckPacket(packet.seq+configPacket.chunksize)
                    clientConnection.sendPacket(ackPacket)

                goodData += packetLen
                fileData += base64.b64decode(packet.data);

                expectedSeq = packet.seq+configPacket.chunksize #Next packet should be packet received.seq + 1

            else: #If incorrect seq was received, reply with ACK(ExpectedSeq)
                #if delay != 0:
                    #sleep(delay/1000)
                #ackPacket = AckPacket(packet.seq+configPacket.chunksize)
                #clientConnection.sendPacket(ackPacket)

                if delay != 0:
                    t = Timer(delay/1000, reply, [packet.seq+configPacket.chunksize, clientConnection])
                    t.start()
                    pending = pending + 1
                else:
                    ackPacket = AckPacket(packet.seq+configPacket.chunksize)
                    clientConnection.sendPacket(ackPacket)


                if settings._verbose:
                    print "[DATA] Incorrect sequence number received (Received: {} Expected: {})".format(packet.seq, expectedSeq)
                incorrectPackets = incorrectPackets + 1
     

        if packet.seq == lastPacketSeq : #ACK signal from server end of file transfer
            elapsedTime = time.time() - startTime

            while pending: 
                pass
            if settings._verbose:
                    print "\n[FILE] Received last packet from sever, terminating file transfer"
                    print "[FILE] Expected to Recieve DATA({}) as last data packet, received DATA({})\n".format(expectedSeq-chunksize,lastPacketSeq)

            print "[RECV] File transfer complete, writing data to file"

            f = open('received_'+filename, 'wb')
            f.write(fileData)
            f.close()

            print "\n========= Transmission Statistics =========="
            print "File Received: {} ({} bytes)".format('received_'+filename, os.path.getsize('received_'+filename))
            print "Elapsed Time: {}".format(secondsToStr(elapsedTime))
            print "Number of Packets Received: {}".format(packetsReceived)
            print "Number of Discarded Packets Received: {}".format(incorrectPackets)
            print "Goodput: {} bytes per second (number of good packets/time)".format(goodData/elapsedTime)
            print "Throughput: {} bytes per second (total number of packets/time)\n".format(totalData/elapsedTime)
            break 

if __name__ == "__main__":
    main(sys.argv[1:])